# Mitt program

Se [oppgaveteksten](./OPPGAVETEKST.md) til semesteroppgave 2. Denne README -filen kan du endre som en del av dokumentasjonen til programmet, hvor du beskriver for en bruker hvordan programmet skal benyttes.

# Tic Tac Toe

My programme is a tic tac toe game with some slightly different rules and played on a 9x9 grid instead of a 3x3 grid. The 9x9 grid consists of 9, 3x3 tic tac toe games that happen simultaneously and the goal is to win as many of the 9 games as possible. With the bigger grid this game requires more strategical thinking than a simple tic tac toe game, but the rules are still the same. 

## How to play
Use the mouse to click on the grid cell of your choice to either mark it as 'x' or 'o'.

# How to run the game
Use your IDE and launch the game from Main.java by clicking run

# Video
[video]https://drive.google.com/file/d/1T6kbH19db2CFzZw_at7A9AmFH51dNHq7/view?usp=sharing
