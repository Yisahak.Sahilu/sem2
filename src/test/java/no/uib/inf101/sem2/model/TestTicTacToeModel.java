package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;

public class TestTicTacToeModel {
    
    @Test
    public void testSetXO() {
        TicTacToeBoard board = new TicTacToeBoard(3, 3);
        TicTacToeModel model = new TicTacToeModel(board);

        assertEquals(false, model.setXO('q', new CellPosition(0, 0)));
        assertEquals(true, model.setXO('x', new CellPosition(0, 0)));
        try {
            @SuppressWarnings("unused")
            boolean bool = model.setXO('x', new CellPosition(3, 3));
            fail();
        } catch (IndexOutOfBoundsException e){
            //test passed
        }

    }

    @Test
    public void testCheckWin() {
        TicTacToeBoard board = new TicTacToeBoard(3, 3);
        TicTacToeModel model = new TicTacToeModel(board);

        model.setXO('x', new CellPosition(0, 0));
        model.setXO('x', new CellPosition(1, 0));
        model.setXO('x', new CellPosition(2, 0));
        
        assertTrue(model.checkWin());
    }


}
