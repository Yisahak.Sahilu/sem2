package no.uib.inf101.sem2.view;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.Color;

import org.junit.jupiter.api.Test;

public class TestTicTacToeColor {
    @Test
    public void sanityTestDefaultColorTheme() {
      ColorTheme colors = new TicTacToeColor();
      assertEquals(Color.BLACK, colors.getBackgroundColor());
      assertEquals(Color.WHITE, colors.getFrameColor());
      
      
    }
}
