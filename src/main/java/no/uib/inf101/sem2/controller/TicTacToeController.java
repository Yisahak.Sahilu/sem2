package no.uib.inf101.sem2.controller;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.view.TicTacToeView;



public class TicTacToeController implements MouseListener {
    
    /**
     * some of the code for the MouseListener was sourced from chatGPT and stack overflow
     */

    private ControllableTicTacToeModel model;
    private TicTacToeView ticTacToeView;
    int cellSize = (int) ((int)ticTacToeView.CELLSIZE + this.ticTacToeView.MARGIN);

    private char player = 'x';
    

    public TicTacToeController(ControllableTicTacToeModel model, TicTacToeView ticTacToeView) {
        this.model = model;
        this.ticTacToeView = ticTacToeView;
        ticTacToeView.addMouseListener(this);
        
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(model.getGameState() == GameState.ACTIVE_GAME) {
            int x = e.getX();
        int y = e.getY();
    
        int cellWidth = cellSize; 
        int cellHeight = cellSize; 
        int row = y / cellHeight;
        int col = x / cellWidth;

        System.out.println("Clicked on cell row: " + row + " and col: " + col);

        
        //if previous move was executed
        if (this.model.setXO(player, new CellPosition(row, col))) {
            player = model.setPlayer(player);
        }
        model.checkWin();
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        Object source = e.getSource();

        
        if (source instanceof TicTacToeView) {
            // Cast the source to TicTacToeView
            TicTacToeView ticTacToeView = (TicTacToeView) source;

            // Set the cursor to a hand cursor
            ticTacToeView.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
        Object source = e.getSource();

        if (source instanceof TicTacToeView) {
            
            TicTacToeView ticTacToeView = (TicTacToeView) source;

            
            ticTacToeView.setCursor(Cursor.getDefaultCursor());
    }
    }

}
