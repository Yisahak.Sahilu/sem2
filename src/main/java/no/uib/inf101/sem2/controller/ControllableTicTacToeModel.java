package no.uib.inf101.sem2.controller;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.model.GameState;

public interface ControllableTicTacToeModel {
    
    /**
     * 
     * @param c the value, either 'x' or 'o'
     * @param pos the cell position
     * @return true of XO has been set on the grid
     */
    boolean setXO(char c, CellPosition pos);
    
    /**
     * 
     * @param c the char value depending on the player: 'x' or 'o'
     * @return the char value for the next player, either from 'x' to 'o' or vice versa
     */
    char setPlayer(char c);

    /**
     * checks if win conditions have been met
     * @return true if there is a winner
     */
    boolean checkWin();

    /**
     * 
     * @return whether game state is active or game over
     */
    GameState getGameState();
    
}
