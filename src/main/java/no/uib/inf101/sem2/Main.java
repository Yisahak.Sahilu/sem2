package no.uib.inf101.sem2;

import no.uib.inf101.sem2.controller.TicTacToeController;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.model.TicTacToeBoard;
import no.uib.inf101.sem2.model.TicTacToeModel;
import no.uib.inf101.sem2.view.SampleView;
import no.uib.inf101.sem2.view.TicTacToeView;

import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    
    TicTacToeBoard ticTacToeBoard = new TicTacToeBoard(9, 9);
    
    TicTacToeModel ticTacToeModel = new TicTacToeModel(ticTacToeBoard);
    TicTacToeView ticTacToeView = new TicTacToeView(ticTacToeModel);

    TicTacToeController ticTacToeController = new TicTacToeController(ticTacToeModel, ticTacToeView);

    SampleView view = new SampleView();
    

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("INF101");
    frame.setContentPane(ticTacToeView);
    frame.pack();
    frame.setVisible(true);
  }
}
