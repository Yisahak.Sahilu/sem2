package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.model.GameState;

public interface ViewableTicTacToeModel {
    
    /**
     * returns a GridDimension object with rows and cols of grid
     * @return
     */
    GridDimension getDimension();

    /**
     * iterates over grid and returns position and value of each coordinate 
     * @return
     */
    Iterable<GridCell<Character>> getTilesOnBoard();

    /**
     * 
     * @return whether game state is active or game over
     */
    GameState getGameState();

}
