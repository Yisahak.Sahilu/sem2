package no.uib.inf101.sem2.view;

import java.awt.Color;

public class TicTacToeColor implements ColorTheme{

    @Override
    public Color getCellColor(char c) {
        Color color = switch(c) {
            case 'r' -> Color.RED;
            case 'g' -> Color.GREEN;
            case 'b' -> Color.BLUE;
            case 'y' -> Color.YELLOW;
            case '-' -> Color.MAGENTA;
            case 'x' -> Color.GRAY;
            case 'o' -> Color.WHITE;
            default -> throw new IllegalArgumentException(
                "No available color for '" + c + "'");
        };

        return color;
    }

    @Override
    public Color getFrameColor() {
        return Color.WHITE;
    }

    @Override
    public Color getBackgroundColor() {
        return Color.BLACK;
    }
 
    @Override
    public Color getTransparentColor() {
        return new Color(0, 0, 0, 128);
    }
}
