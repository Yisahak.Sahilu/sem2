package no.uib.inf101.sem2.view;

import javax.swing.JPanel;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.TicTacToeModel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class TicTacToeView  extends JPanel {
    
    ViewableTicTacToeModel viewableTicTacToeModel;
    ColorTheme colorTheme;
    CellPositionToPixelConverter converter;

    public static final double MARGIN = 2;
    public static final double CELLSIZE = 50;
    private static final double WIDTH = (CELLSIZE + MARGIN) * 9;
    private static final double HEIGHT = (CELLSIZE + MARGIN) * 9;

    public TicTacToeView(ViewableTicTacToeModel viewableTicTacToeModel) {
        this.viewableTicTacToeModel = viewableTicTacToeModel;
        this.colorTheme = new TicTacToeColor();
        this.setBackground(colorTheme.getBackgroundColor());
        this.setPreferredSize(new Dimension((int) WIDTH, (int) HEIGHT));
    }


    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        //draw game here
        drawGame(g2);
        if(viewableTicTacToeModel.getGameState() == GameState.GAME_OVER){
            drawGameOver(g2);
        }

    }

    private void drawGameOver(Graphics2D g2) {
        g2.setColor(colorTheme.getTransparentColor());
        Rectangle2D rect = new Rectangle2D.Double(MARGIN, MARGIN, WIDTH, HEIGHT);
        g2.fill(rect);
    }


    private void drawGame(Graphics2D g2) {
        g2.setColor(Color.BLACK);

        Rectangle2D rect = new Rectangle2D.Double(MARGIN, MARGIN, WIDTH, HEIGHT);
        g2.fill(rect);

        CellPositionToPixelConverter converter = new CellPositionToPixelConverter(rect, viewableTicTacToeModel.getDimension(), MARGIN);
        drawCells(g2, viewableTicTacToeModel.getTilesOnBoard(), converter, colorTheme);
        this.repaint();
    }

    private static void drawCells(Graphics2D g2, Iterable<GridCell<Character>> cells, 
                                CellPositionToPixelConverter converter, ColorTheme colorTheme) {

        for (GridCell<Character> cell : cells){
            Rectangle2D rect = converter.getBoundsForCell(cell.pos());
            Color color = colorTheme.getCellColor(cell.value());
            g2.setColor(color);
            g2.fill(rect);

            //Write XO on board
            //Set the font size
            g2.setColor(Color.BLACK);
            g2.setFont(new Font(g2.getFont().getName(), g2.getFont().getStyle(), 50));
            
            String text = String.valueOf(cell.value());
            
            //skip if cell value is not set to either 'x' or 'o'
            if (cell.value() == '-') {
                continue;
            }

            FontMetrics fontMetrics = g2.getFontMetrics();
            int x = (int) (cell.pos().col() * (CELLSIZE + MARGIN) + (CELLSIZE - fontMetrics.stringWidth(text)) / 2);
            int y = (int) (cell.pos().row() * (CELLSIZE + MARGIN) + (CELLSIZE - fontMetrics.getHeight()) / 2 + fontMetrics.getAscent());
            
            g2.drawString(text, x, y);

            
        }
    }

}
