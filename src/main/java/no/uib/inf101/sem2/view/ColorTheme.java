package no.uib.inf101.sem2.view;

import java.awt.Color;

public interface ColorTheme {
    
    /**
     * 
     * @param c, char indicating which color, returns a color from char input
     * @return
     */
    Color getCellColor(char c);

    /**
     * sets frame color
     * @return
     */
    Color getFrameColor();

    /**
     * sets background color
     * @return
     */
    Color getBackgroundColor();

    /**
     * 
     * @return gets transparent gameover screen color
     */
    Color getTransparentColor();
}
