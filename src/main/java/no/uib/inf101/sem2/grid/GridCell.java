package no.uib.inf101.sem2.grid;

/**
 * A Gridcell consists of a cellPostion and a value.
 *
 * @param pos  the position of the cell
 * @param value the value of char indicating the color of the cell
 */
public record GridCell<E>(CellPosition pos, E value) {}
