package no.uib.inf101.sem2.grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Grid<E> implements IGrid<E>{
    private final int rows;
    private final int cols;
    private final List<E> cells;
    
    public Grid(int rows, int cols) {
        if (rows <= 0 || cols <= 0) {
			throw new IllegalArgumentException();
		}

        this.rows = rows;
		this.cols = cols;
		this.cells = new ArrayList<>(cols * rows);
        for (int i = 0; i < cols * rows; i++) {
			this.cells.add(null);
		}
    }

    public Grid(int rows, int cols, E initElement) {
		if (rows <= 0 || cols <= 0) {
			throw new IllegalArgumentException();
		}

        this.rows = rows;
		this.cols = cols;
		this.cells = new ArrayList<>(cols * rows);
		for (int i = 0; i < cols * rows; i++) {
			this.cells.add(initElement);
		}
	}



    @Override
    public int rows() {
        return this.rows;
    }

    @Override
    public int cols() {
        return this.cols;
    }

    @Override
    public Iterator<GridCell<E>> iterator() {
        ArrayList<GridCell<E>> list = new ArrayList<>(rows() * cols());

        for(int i = 0; i < rows(); i++){
            for(int j = 0; j < cols(); j++){
                CellPosition pos = new CellPosition(i, j);
                list.add(new GridCell<E>(pos, get(pos)));
            }
        }

        return list.iterator();
    }

    @Override
    public void set(CellPosition pos, E value) {
        if(!this.positionIsOnGrid(pos)){
            throw new IndexOutOfBoundsException("Row and col must be within grid bounds.");
        }
        this.cells.set(coordinateToIndex(pos), value);
    }

    @Override
    public E get(CellPosition pos) {
        if(!this.positionIsOnGrid(pos)){
            throw new IndexOutOfBoundsException("Row and col must be within grid bounds.");
        }

        return this.cells.get(coordinateToIndex(pos));
    }

    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        return pos.row() >= 0 && pos.row() < this.rows()
            && pos.col() >= 0 && pos.col() < this.cols();
    }
    

    /**
     * converts position coordinates into index 
     * @param pos
     * @return index
     */
    private int coordinateToIndex(CellPosition pos) {
		return pos.row() + pos.col() * rows;
	}



}
