package no.uib.inf101.sem2.grid;


/**
 * represents a position on the grid
 * @param row the row of the cell
 * @param col the col of the cell
 */
public record CellPosition(int row, int col) {}
