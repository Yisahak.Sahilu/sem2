package no.uib.inf101.sem2.model;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import no.uib.inf101.sem2.controller.ControllableTicTacToeModel;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.view.ViewableTicTacToeModel;

public class TicTacToeModel implements ViewableTicTacToeModel, ControllableTicTacToeModel{

    private TicTacToeBoard ticTacToeBoard;
    private ArrayList<CellPosition> miniGrids;
    private GameState gameState;

    int xWins = 0;
    int oWins = 0;

    public TicTacToeModel(TicTacToeBoard ticTacToeBoard) {
        this.ticTacToeBoard = ticTacToeBoard;
        this.miniGrids = ticTacToeBoard.getMiniGridPos();
        this.gameState = GameState.ACTIVE_GAME;
    }

    @Override
    public GridDimension getDimension() {
        return new Grid<>(ticTacToeBoard.rows(), ticTacToeBoard.cols());
    }

    @Override
    public Iterable<GridCell<Character>> getTilesOnBoard() {
        return this.ticTacToeBoard;
    }

    @Override
    public boolean setXO(char c, CellPosition pos) {
        if(pos.row() < 0 || pos.col() < 0 || pos.row() >= getDimension().rows() || pos.col() >= getDimension().cols()){
            throw new IndexOutOfBoundsException("Position: (" + pos.row() + ", " + pos.col() + ") not on grid.");
        }
        if (c == 'x' || c == 'o' ){
            if (ticTacToeBoard.get(pos) != '-') {
                return false;
            }
            ticTacToeBoard.set(pos, c);
            return true;
        } else {
            return false;
        }
        
    }

    @Override
    public char setPlayer(char c) {
        char nextPlayer = c;
        if (c == 'x') {
            nextPlayer = 'o';
        } else if (c == 'o') {
            nextPlayer = 'x';
        }

        return nextPlayer;
    }

    @Override
    public boolean checkWin() {
        
        for (int i = 0; i < miniGrids.size(); i++) {
            char winner = checkSmallWin(miniGrids.get(i));
            if (winner != '-') {
                if (winner == 'x'){
                    xWins++;
                } else if (winner == 'o') {
                    oWins++;
                } else if (winner == 'N') {
                    System.out.println("No winner, full mini grid!");
                }
                System.out.println("X wins: " + xWins + "; O wins: " + oWins);
                this.miniGrids.remove(i);
                if (miniGrids.isEmpty()) {
                    System.out.println("GAME OVER!");
                    this.gameState = GameState.GAME_OVER;
                    if (xWins > oWins) {
                        JOptionPane.showMessageDialog(null, "Game Over! Player X wins.");
                    } else if (oWins > xWins) {
                        JOptionPane.showMessageDialog(null, "Game Over! Player O wins.");
                    } else {
                        JOptionPane.showMessageDialog(null, "Game Over! It's a tie.");
                    }
                    
                }
                return true;
            } 

        }


        return false;
        
    }

    
    //checks for winner in the mini tic tac toe games
    private char checkSmallWin(CellPosition pos) {
        int row = pos.row();
        int col = pos.col();

        //check rows
        for (int i = 0; i < 3; i++) {
            if (ticTacToeBoard.get(new CellPosition(row + i, col)) == ticTacToeBoard.get(new CellPosition(row + i, col + 1))
            && ticTacToeBoard.get(new CellPosition(row + i, col + 1)) == ticTacToeBoard.get(new CellPosition(row + i, col + 2))
            && ticTacToeBoard.get(new CellPosition(row + i, col)) != '-') {
                System.out.println("there is a winner with rows");
                return ticTacToeBoard.get(new CellPosition(row + i, col));
            }
            
        }

        //check cols
        for (int i = 0; i < 3; i++) {
            if (ticTacToeBoard.get(new CellPosition(row, col + i)) == ticTacToeBoard.get(new CellPosition(row + 1, col + i))
            && ticTacToeBoard.get(new CellPosition(row + 1, col + i)) == ticTacToeBoard.get(new CellPosition(row + 2, col + i))
            && ticTacToeBoard.get(new CellPosition(row, col + i)) != '-') {
                System.out.println("there is a winner with cols");
                return ticTacToeBoard.get(new CellPosition(row, col + i));
            }
            
        }

        //check diagonals
        if (ticTacToeBoard.get(new CellPosition(row + 2, col)) == ticTacToeBoard.get(new CellPosition(row + 1, col + 1))
        && ticTacToeBoard.get(new CellPosition(row + 1, col + 1)) == ticTacToeBoard.get(new CellPosition(row, col + 2))
        && ticTacToeBoard.get(new CellPosition(row + 2, col)) != '-') {
            System.out.println("there is a winner with diagonal 1");
            return ticTacToeBoard.get(new CellPosition(row + 2, col));
        }

        if (ticTacToeBoard.get(new CellPosition(row, col)) == ticTacToeBoard.get(new CellPosition(row + 1, col + 1))
        && ticTacToeBoard.get(new CellPosition(row + 1, col + 1)) == ticTacToeBoard.get(new CellPosition(row + 2, col + 2))
        && ticTacToeBoard.get(new CellPosition(row, col)) != '-') {
            System.out.println("there is a winner with diagonal 2");
            return ticTacToeBoard.get(new CellPosition(row, col));
        }

        //check if there is no winner, return 'N'
        if (ticTacToeBoard.get(new CellPosition(row, col)) != '-' 
        && ticTacToeBoard.get(new CellPosition(row, col + 1)) != '-'
        && ticTacToeBoard.get(new CellPosition(row, col + 2)) != '-'
        && ticTacToeBoard.get(new CellPosition(row + 1, col)) != '-' 
        && ticTacToeBoard.get(new CellPosition(row + 1, col + 1)) != '-'
        && ticTacToeBoard.get(new CellPosition(row + 1, col + 2)) != '-'
        && ticTacToeBoard.get(new CellPosition(row + 2, col)) != '-' 
        && ticTacToeBoard.get(new CellPosition(row + 2, col + 1)) != '-'
        && ticTacToeBoard.get(new CellPosition(row + 2, col + 2)) != '-') {
            return 'N';
        }

        return '-';

    }

    @Override
    public GameState getGameState() {
        return this.gameState;
    }

    

    
    
}
