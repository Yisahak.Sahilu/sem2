package no.uib.inf101.sem2.model;

import java.util.ArrayList;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;

public class TicTacToeBoard extends Grid<Character> {
    TicTacToeBoard ticTacToeBoard;
    int rows;
    int cols;

    public TicTacToeBoard(int rows, int cols) {
        super(rows, cols, '-');
        this.rows = rows;
        this.cols = cols;
        this.ticTacToeBoard = this;
    }

    //returns the top left cell position of each mini tic tac toe grid
    public ArrayList<CellPosition> getMiniGridPos() {
        ArrayList<CellPosition> miniGrids = new ArrayList<>(9);

        miniGrids.add(new CellPosition(0, 0));
        miniGrids.add(new CellPosition(0, 3));
        miniGrids.add(new CellPosition(0, 6));
        miniGrids.add(new CellPosition(3, 0));
        miniGrids.add(new CellPosition(3, 3));
        miniGrids.add(new CellPosition(3, 6));
        miniGrids.add(new CellPosition(6, 0));
        miniGrids.add(new CellPosition(6, 3));
        miniGrids.add(new CellPosition(6, 6));

        return miniGrids;
    }
}
